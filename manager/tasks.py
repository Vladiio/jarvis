import os

from celery import Celery
from celery.schedules import crontab

import requests


TELEGRAM_TOKEN = os.environ.get('TELEGRAM_TOKEN')
API_URL = 'https://api.telegram.org/'
CHAT_ID = 274965384

app = Celery('tasks', broker='amqp://rabbitmq')
app.conf.timezone = 'Europe/Kiev'


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(
        crontab(hour=20, minute=57),
        say_good_morning.s()
    )


@app.task
def say_good_morning():
    url = f'{API_URL}bot{TELEGRAM_TOKEN}/sendMessage'
    data = dict(
        chat_id=CHAT_ID,
        text='Good morning, sir.'
    )
    requests.post(url, data=data)
