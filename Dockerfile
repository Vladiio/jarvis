FROM python:3.6.8-alpine3.8

RUN apk add gcc musl-dev libffi-dev openssl-dev python3-dev
ADD . .
RUN pip install -r requirements/develop.txt

RUN chmod +x start.sh

ENTRYPOINT [ "./start.sh" ]