import base64
import json
import os

import boto3
from botocore.exceptions import ClientError
from fabric import task


def get_secrets():

    secret_name = "prod/docker"
    region_name = "eu-central-1"
    secret = None

    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager', region_name=region_name)

    # In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
    # See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
    # We rethrow the exception by default.

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name)
    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            # An error occurred on the server side.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            # You provided an invalid value for a parameter.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            # You provided a parameter value that is not valid for the current state of the resource.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            # We can't find the resource that you asked for.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
    else:
        # Decrypts secret using the associated KMS CMK.
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']

        else:
            decoded_binary_secret = base64.b64decode(
                get_secret_value_response['SecretBinary'])
    return secret


@task
def deploy(c):
    c.put('build.sh', '/home/ubuntu/')
    secrets = get_secrets()
    parsed_secrets = None
    if secrets:
        parsed_secrets = json.loads(secrets)
    # with open('environment.sh', 'w+') as f:
    #     for name, value in parsed_secrets.items():
    #         f.write(f'\n{name}={value}')
    # c.put('environment.sh', '/home/ubuntu/')
    # c.run('sudo sh /home/ubuntu/build.sh', env=parsed_secrets, replace_env=True)
    c.run('sudo docker login -u {} -p {}'.format(*parsed_secrets.values()))
    c.run('docker pull vladio/jarvis:latest')
    c.run('docker run vladio/jarvis:latest')
    
