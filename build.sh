#! /bin/bash

# INSTALL DOCKER
source /home/ubuntu/environment.sh

apt-get update -y

apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common -y

curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
apt-get update -y && apt-get install docker-ce docker-ce-cli containerd.io -y

# echo $DOCKER_PASSWORD
# echo $DOCKER_USERNAME
# docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD